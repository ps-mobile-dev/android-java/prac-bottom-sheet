package com.shankar.bottomsheettest;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.TextView;

import com.mahc.custombottomsheetbehavior.BottomSheetBehaviorGoogleMapsLike;
import com.mahc.custombottomsheetbehavior.MergedAppBarLayout;
import com.mahc.custombottomsheetbehavior.MergedAppBarLayoutBehavior;

public class MainActivity extends AppCompatActivity {

    private TextView textView;

    private MergedAppBarLayoutBehavior mergedAppBarLayoutBehavior;
    private  MergedAppBarLayout mergedAppBarLayout;
private BottomSheetBehaviorGoogleMapsLike behaviorGoogleMapsLike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text);

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorlayout);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);   //a ScrollView inside the coordinatorLayout
        behaviorGoogleMapsLike  = BottomSheetBehaviorGoogleMapsLike.from(bottomSheet);
        behaviorGoogleMapsLike.addBottomSheetCallback(new BottomSheetBehaviorGoogleMapsLike.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState){
                    case BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED:
                        Log.d("MainActivity", "Collapsed");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_DRAGGING:
                        Log.d("MainActivity", "Dragging");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_EXPANDED:
                        Log.d("MainActivity", "Expanded");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT:
                        Log.d("MainActivity", "State Anchor Point");
                        break;
                    case BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN:
                        Log.d("MainActivity", "Hidden");
                        break;
                    default:
                        Log.d("MainActivity", "Settling");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        mergedAppBarLayout = findViewById(R.id.mergedappbarlayout);
        mergedAppBarLayoutBehavior = MergedAppBarLayoutBehavior.from(mergedAppBarLayout);
        mergedAppBarLayoutBehavior.setToolbarTitle("Toilet");
        mergedAppBarLayoutBehavior.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behaviorGoogleMapsLike.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
            }
        });

        textView = (TextView) bottomSheet.findViewById(R.id.btmSheetServiceName);
        textView.setText("Toilet");
        //adapter
        behaviorGoogleMapsLike.setState(BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT);
    }

    @Override
    public void onBackPressed() {
        if(behaviorGoogleMapsLike.getState() != BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED){
            behaviorGoogleMapsLike.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
        }
        else {
            super.onBackPressed();
        }
    }
}
